Python/Flask blog app that has served me for the past decade or so. Used to run standalone behind an nginx proxy, nowadays its handled by gunicorn and a
systemd socket activation.
