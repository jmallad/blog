#!/usr/bin/env python

from aiohttp import web
from aiohttp_wsgi import WSGIHandler
from flask import Flask, render_template, abort, session, request, url_for, redirect, Markup
import core, config, os, hashlib, uuid, time, traceback, werkzeug, sys, subprocess, pickle, json, datetime
from urllib.parse import urlparse
from werkzeug.utils import secure_filename

def update_secret_key(app):
    app.secret_key = hashlib.sha512(str(uuid.uuid4()).encode('utf-8') + str(uuid.uuid4()).encode('utf-8')).hexdigest()
    
#Static redirects
def route_static_redirects(app):
    if not os.path.exists("static-redirects.txt"):
        return
    data = open("static-redirects.txt").read()
    data = data.replace("\r","").split("\n")
    for line in data:
        if not line:
            break
        a, b = tuple(line.split(" "))
        app.add_url_rule(a, b, lambda: werkzeug.utils.redirect(b))


#Initialize database
db = core.postdb()
db.connect(config.db_url, config.db_key)

#Flask app definition
app = Flask(__name__)

#Flask routes
@app.route("/")
def index():
    return redirect("/page/1")
@app.route("/tag/<tag>")
def show_tag(tag):
    return redirect("/tag/{}/1".format(tag))
@app.route("/post/<postid>")
def show_post(postid):
    path = os.path.join("post/", postid)
    if not os.path.exists(path):
        abort(404)
    body = open(path).read()
    post = db.post_get(postid)
    return render_template("post.html", title=config.site_title, posttitle=post[2],
                           body=Markup(body), tags=post[3], asset_url=config.asset_url,
                           created=post[1])
@app.route("/tag/<tag>/<page>")
def get_tag_page(tag, page):
    offset = core.page_offset(page)
    posts = db.tag_search(tag)
    if not posts:
        abort(404)
    pages = core.posts_pages(posts)
    return render_template("index.html", posts=posts[offset:offset+5],
                           prefix="/tag/{}/".format(tag), pages=pages, query="",
                           title="{} - tag \"{}\" page {}".format(config.site_title, tag, page),
                           asset_url=config.asset_url)
@app.route("/page/<page>")
def get_page(page):
    offset = core.page_offset(page)
    if page == 1:
        title=config.site_title
    else:
        title="{} - page {}".format(config.site_title, page)
    pages = int(db.postmax / 5) + 1
    return render_template("index.html", posts=db.post_list(offset,5),
                           prefix="/page/", pages=pages,
                           query="", title=title, asset_url=config.asset_url)
@app.route("/search/<page>")
def search(page):
    offset = core.page_offset(page)
    try:
        posts = db.post_search(request.args.get("type", "tags"), request.args.get("function", "or"), request.args.get("query", ""), offset, 5)
    except:
        traceback.print_exc()
        abort(500)
    if not posts:
        return render_template("noresults.html", title="{} - search".format(config.site_title, page), asset_url=config.asset_url)
    if request.args.get("type", "tags") == "fulltext":
        pages=int((db.postmax/5))+1
    else:
        pages=core.posts_pages(posts)
        posts=posts[offset:offset+5]
    return render_template("index.html", posts=posts, prefix="/search/",
                           pages=pages, query=urlparse(request.url).query,
                           title="{} - search - page {}".format(config.site_title, page),
                           asset_url=config.asset_url)
######## **** API endpoints **** ########
# Version 3
#######  *********************** #######
@app.route("/v3/verify", methods=["POST"])
def remote_verify_v3():
    try:
        if request.form["key"] != config.api_key:
            return json.dumps((401, "Unauthorized"))
        return json.dumps((200, "OK"))
    except:
        traceback.print_exc()
        return json.dumps((400, "Bad Request"))
@app.route("/v3/list", methods=["POST"])
def remote_list_v3():
    try:
        if request.form["key"] != config.api_key:
            return json.dumps((401, "Unauthorized"))
        offset = int(request.form["offset"])
        limit = int(request.form["limit"])
        try:
            dump = []
            for post in db.post_list(offset, limit):
                dump.append({"postid":str(post[0]),
                             "created":time.mktime(post[1].timetuple()),
                             "title":post[2],
                             "tags":post[3]})
            return json.dumps((200, dump))
        except:
            traceback.print_exc()
            return json.dumps((500, "Internal"))
    except:
        traceback.print_exc()
        return json.dumps((400, "Bad Request"))
@app.route("/v3/select", methods=["POST"])
def remote_select_v3():
    try:
        if request.form["key"] != config.api_key:
            return json.dumps((401, "Unauthorized"))
        query = request.form["query"]
        try:
            print("[API] /v3/select: executing query: {}".format(query))
            ret = db.select(query)
            final = []
            for res in ret:
                row = []
                for i, val in enumerate(res):
                    if type(val) == datetime.datetime:
                        row.append(int(time.mktime(val.timetuple())))
                    elif type(val) == uuid.UUID:
                        row.append(str(val))
                    else:
                        row.append(val)
                final.append(row)
            return json.dumps((200, final))
        except:
            traceback.print_exc()
            return json.dumps((500, "Internal"))
    except:
        traceback.print_exc()
        return json.dumps((400, "Bad Request"))
@app.route("/v3/rm", methods=["POST"])
def remote_rm_v3():
    try:
        if request.form["key"] != config.api_key:
            return json.dumps((401, "Unauthorized"))
        public = request.form["public"]
        path = core.file_path(request.form["path"], public)
        try:
            print("[API] /v3/rm: removing file \"{}\"".format(path))
            os.remove(path)
            return json.dumps((200, "OK"))
        except:
            traceback.print_exc()
            return json.dumps((500, "Internal"))
    except:
        traceback.print_exc()
        return json.dumps((400, "Bad Request"))
@app.route("/v3/clear", methods=["POST"])
def remote_clear_v3():
    try:
        if request.form["key"] != config.api_key:
            return json.dumps((401, "Unauthorized"))
        try:
            posts = db.select("SELECT id FROM posts")
            for post in posts:
                print(post)
                os.remove(os.path.join("post/", str(post[0])))
            db.execute("DELETE FROM tags")
            db.execute("DELETE FROM posts")
            return json.dumps((200, "OK"))
        except:
            traceback.print_exc()
            return json.dumps((500, "Internal"))
    except:
        traceback.print_exc()
        return json.dumps((400, "Bad Request"))
@app.route("/v3/push", methods=["POST"])
def remote_push_v3():
    try:
        if request.form["key"] != config.api_key:
            return json.dumps((401, "Unauthorized"))
        title = request.form["title"]
        tags = request.form["tags"].split(" ")
        body = request.form["body"]
        try:
            print("[API] /v3/push: pushing new post with title \"{}\"".format(title))
            postid = db.post_put(title, body, tags)
            return json.dumps((200, postid))
        except:
            traceback.print_exc()
            return json.dumps((500, "Internal"))
    except:
        traceback.print_exc()
        return json.dumps((400, "Bad Request"))
@app.route("/v3/pop", methods=["POST"])
def remote_pop_v3():
    try:
        if request.form["key"] != config.api_key:
            return json.dumps((401, "Unauthorized"))
        postid = request.form["postid"]
        try:
            print("[API] /v3/pop: deleting post id \"{}\"".format(postid))
            db.post_pop(postid)
            return json.dumps((200, postid))
        except:
            traceback.print_exc()
            return json.dumps((500, "Internal"))
    except:
        traceback.print_exc()
        return json.dumps((400, "Bad Request"))

update_secret_key(app)
route_static_redirects(app)

#WSGI bridge 
wsgi = WSGIHandler(app)
aio = web.Application()

#AIOHTTP routes
aio.router.add_route("*", "/{path_info:.*}", wsgi)

print("[info] initialized successfully")

