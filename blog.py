#!/usr/bin/env python

from aiohttp import web

def index(req):
    return web.Response(text="hello")

app = web.Application()
app.router.add_get('/', index)
