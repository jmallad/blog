#!/usr/bin/env python

import os, psycopg2, uuid, psycopg2.extras, config
from werkzeug.utils import secure_filename
from db.client import remote_client
psycopg2.extras.register_uuid()
#import bcrypt

'''
SQLite db stores post titles, dates, and id numbers
posts are stored in text files under posts/<post id>
tag database is pickled as a dict of the format {"<tag>": [<post id>, <post id>, ...], ...}
'''

def gen_uuid(path):
    ret = uuid.uuid4()
    if os.path.exists(os.path.join(path, str(ret))):
        return gen_uuid(path)
    return ret

#def auth(pwd):
#    hashed = bcrypt.kdf(password=pwd.encode('utf-8'), salt=config.auth_salt, desired_key_bytes=64, rounds=300)
#    if hashed != config.auth_hash:
#        return True
#    return False

def page_offset(page):
    if int(page) > 1:
        offset = ((int(page)-1)*5)
    else:
        offset = 0
    return offset

def posts_pages(posts):
    if not len(posts) or not (len(posts) / 5) or len(posts) == 5:
        return 1
    return int((len(posts) / 5)+1)

def file_path(path, public):
    if public == "true":
        path = "/www/assets/{}".format(secure_filename(path))
    else:
        path = "/www/assets/private/{}".format(secure_filename(path))    
    return path

class postdb():
    def __init__(self):
        self.dbcur = None
        self.dbconn = None
        self.postmax = 0
    def connect(self, url, key):
        self.dbconn = remote_client.apiclient_v3(url, key)
        self.post_count()
    def execute(self, query):        
        ret = self.dbconn.execute(query)
        print("---------------")
        print(query)
        print(ret)
        print("---------------")                                       
        if ret[0] != 200:
            self.rollback()
            raise SystemError("SQL API select call failed")
        return True
    def select(self, query):
        ret = self.dbconn.select(query)
        print("---------------")
        print(query)
        print(ret)
        print("---------------")                                        
        if ret[0] != 200:
            raise SystemError("SQL API select call failed: {}".format(ret))
        if ret[1]:
            return ret[1][0]
        return []
    def begin(self):
        self.execute("BEGIN")
    def commit(self):
        self.execute("COMMIT")
    def rollback(self):
        self.execute("ROLLBACK")
    def post_count(self):
        ret = self.select("SELECT COUNT(*) FROM posts")
        if ret:
            self.postmax = int(ret[0])
            return self.postmax
        self.postmax = 0
        return self.postmax
    def post_list(self, offset=0, limit=0):
        posts = self.select("SELECT * FROM posts OFFSET {} LIMIT {}".format(
            offset, limit))
        if not posts:
            return []
        ret = []
        for post in posts[1]:
            print("POST:" + str(post))
            res = self.select("SELECT tag FROM tags WHERE postid={}".format(
                post[0]))
            tags = [e[0] for e in res]
            ret.append((post[0],post[1],post[2],tags))
        return ret
    def post_put(self, title, body, tags):
        postid = gen_uuid("post/")
        open(os.path.join("post/", str(postid)), "w").write(body)
        self.begin()
        self.execute('''
        INSERT INTO posts VALUES ({}, current_timestamp, {}) 
        '''.format(postid, title))
        for tag in tags:
            self.execute('''
            INSERT INTO tags VALUES ({}, {})
            '''.format(tag, postid))
        self.commit()
        return str(postid)
    def post_pop(self, postid):
        self.begin()
        self.execute("DELETE FROM tags WHERE postid='{}'".format(postid))
        self.execute("DELETE FROM posts WHERE id='{}'".format(postid))
        self.commit()
        os.remove(os.path.join("post/", postid))
    def post_title(self, postid):
        return self.select('''
            SELECT title FROM posts WHERE id={}
            '''.format(postid))
    def post_tags(self, postid):
        ret = self.select('''
            SELECT tag FROM tags WHERE postid={}
            '''.format(postid))
        return [e[0] for e in ret[1]]
    def post_get(self, postid):
        ret = self.select('''
            SELECT created,title FROM posts WHERE id={}
            '''.format(postid))[0]
        tags = [e[0] for e in self.select('''
            SELECT tag FROM tags WHERE postid={}
            '''.format(postid))]
        return (postid, ret[0], ret[1], tags)
    def tag_search(self, tag):
        ret = self.select('''
            SELECT postid FROM tags WHERE tag={}
            '''.format(tag))
        posts = []
        if not ret:
            return None
        for postid in ret:
            posts.append(self.post_get(postid[0]))
        return posts
    #type == tags, function == and
    def post_search_1(self, query):
        posts = []
        results = []
        tags = query.split(" ")
        for tag in tags:
            ret = self.select("SELECT postid FROM tags WHERE tag={}".format(
                tag))
            if ret:
                results.append(ret)
        if not results:
            return None
        if len(results) < len(tags):
            return None
        for post in results[0]:
            for res in results:
                if post not in res:
                    results[0].remove(post)
        for post in results[0]:
            posts.append(self.post_get(post[0]))
        return posts
    #type == tags, function == or
    def post_search_2(self, query):
        posts = []
        results = []
        postids = []
        tags = query.split(" ")
        for tag in tags:
            ret = self.select("SELECT postid FROM tags WHERE tag={}".format(
                tag))
            if ret:
                results.append(ret)
        if not results:
            return None
        for res in results:
            for post in res:
                if post not in postids:
                    postids.append(post)
        for post in postids:
            posts.append(self.post_get(post[0]))
        return posts
    #type == fulltext, function == and
    def post_search_3(self, query, offset=0, limit=0):
        posts = []
        ret = self.select("SELECT id FROM posts OFFSET {} LIMIT {}".format(
            offset, limit))
        if not ret:
            return None
        tags = query.split(" ")
        for post in ret:
            words = open(os.path.join("post/",str(post[0]))).read()#.replace("\n"," ").replace("\r"," ")
            for tag in tags:
                if tag not in words:
                    ret.remove(post)
        for post in ret:
            posts.append(self.post_get(post[0]))
        return posts
    #type == fulltext, function == or
    def post_search_4(self, query, offset=0, limit=0):
        postids = []
        posts = []
        self.execute("SELECT id FROM posts OFFSET {} LIMIT {}".format(
            offset, limit))
        ret = self.dbcur.fetchall()
        if not ret:
            return None
        tags = query.split(" ")
        for post in ret:
            words = open(os.path.join("post/",str(post[0]))).read()#.replace("\n"," ").replace("\r"," ")
            for tag in tags:
                if tag in words:
                    postids.append(post[0])
                    break
        for post in postids:
            posts.append(self.post_get(post))
        return posts
    #post search wrapper passes arguments to correct subroutine
    def post_search(self, stype, sfunc, query, offset=0, limit=0):
        if query.count(" ") >= config.query_max_terms:
            trunc = query.split(" ", config.query_max_terms)
            del trunc[config.query_max_terms]
            query = " ".join(trunc)
        if stype == "tags" and sfunc == "and":
            return self.post_search_1(query)
        elif stype == "tags" and sfunc == "or":
            return self.post_search_2(query)
        elif stype == "fulltext" and sfunc == "and":
            return self.post_search_3(query, offset, limit)
        elif stype == "fulltext" and sfunc == "or":
            return self.post_search_4(query, offset, limit)
        return None

