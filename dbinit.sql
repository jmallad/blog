CREATE TABLE posts (id uuid primary key, created timestamp, title text);
CREATE TABLE tags (tag text, postid uuid references posts(id));
GRANT ALL ON posts TO blog;
GRANT ALL ON tags TO blog;
set datestyle = 'POSTGRES';

